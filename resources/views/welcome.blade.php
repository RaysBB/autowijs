<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        <div class="p-4 container mx-auto text-center">
            <h1 class="text-4xl">{{ $advertentie->title }}</h1>
            <a class="hover:underline" href="{{ $advertentie->url }}" target="_blank">Naar de advertentie</a>
            @foreach($advertentie->images as $image)
                <div class="pt-6 h-48 w-48 mx-auto">
                    <img class=" h-full w-full object-cover" src="https://{{ $image->image_url }}" alt="auto">
                </div>
            @endforeach
        </div>
    </body>
</html>
