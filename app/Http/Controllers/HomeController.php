<?php

namespace App\Http\Controllers;

use App\Advertentie;
use App\Image;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $advertentie = Advertentie::with('images')->first();

        return view('welcome', compact('advertentie'));
    }
}
