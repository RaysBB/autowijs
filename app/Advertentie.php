<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertentie extends Model
{
    protected $primaryKey = 'advertentie_id';

    protected $keyType = 'varchar';

    public function images()
    {
        return $this->hasMany(Image::class, 'advertentie_id', 'advertentie_id');
    }
}
