<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function advertentie()
    {
        return $this->belongsTo(Advertentie::class, 'advertentie_id', 'advertentie_id');
    }
}
